// modules =================================================
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');

const http = require('http');

const fs = require('fs');
const path = require('path');

dotenv = require('dotenv')

const envLoadResult = dotenv.config()
 
if (envLoadResult.error) {
  console.log("Using env var for configuration")
}
else {
  console.log("Using .env for configuration")
}

if(process.env.NODE_ENV !== 'production' && !envLoadResult.error){ 
    console.log(envLoadResult.parsed);
}

const app = express();


if(process.env.USE_PROXY){
    app.set('trust proxy', true);
}

console.log('Application started');


const httpServer = http.createServer(app);

// configuration

mongoose.connect(process.env.MONGO_URI || 'mongodb://localhost/bop',{ useUnifiedTopology: true,useNewUrlParser: true }); 

if(process.env.NODE_ENV === 'production'){ 
    app.use(morgan('combined'));
}
else {
    app.use(morgan('combined'));
}
app.use(require('cookie-parser')());

app.use((req, res, next)=>{ 
   req.ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress;
    next();
});


app.use('/', express.static(__dirname + '/frontend'));

app.use('/',require('./middleware/tequila'),require('./routes/echange'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = {status_code: 404, status: "Error - Not Found", message: "Url not found : "+req.originalUrl};
    next(err);
});

app.use((err, req, res, next) => {
  res.status(err.status_code || 500).json(err);
});

httpServer.listen(process.env.PORT || 3000, () => console.info("http server started listening"));

module.exports = app;

