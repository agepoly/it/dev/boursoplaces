FROM node:14.9-stretch

WORKDIR /usr/src/app

COPY package*.json ./

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 4B7C549A058F8B6B && (echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.2 main" | tee /etc/apt/sources.list.d/mongodb.list) && apt update && apt install libcurl3 && apt install mongodb-org -y && apt clean
RUN npm install

COPY . .

ENV MONGO_URI=mongodb://localhost:27017/bourse

CMD /bin/bash -c 'mongod --fork --dbpath /data/mongodb/ --logpath /dev/stdout && node server.js'
