const express = require('express');
const router = express.Router();

const Echange = require('../models/echange');

const em = require('../middleware/exchg');


router.get('/echange', em.requireAuth, 
                       em.requireEXCHANGE_ENABLED, 
                       em.requireValidGroup, 
                       em.requireNoPreviousExchange, async (req, res, next)=>

  Echange.startSession().then( async (session) => {
    session.startTransaction();

    let echange = new Echange({target: req.query.target, email: req.tequila.email, sciper: req.tequila.sciper, section: req.tequila.institute, status: 'WISH', previous_group: req.tequila.group});
    let echange_counterpart = await Echange.find({target: req.tequila.group, section: req.tequila.institute, status: "WISH"}).sort({arrivedAt:-1}).limit(1).exec();

    if (echange_counterpart.length == 1) {
      echange.status = echange_counterpart[0].status = "DONE";
      echange.to_notify = echange_counterpart[0].to_notify = 1;

      echange_counterpart[0].counterpart = echange.sciper;
      echange.counterpart = echange_counterpart[0].sciper;

      await echange_counterpart[0].save();
    }
    await echange.save();

    session.endSession();
    return echange;
  })
  .then((echange)=>res.json(echange))
  .catch(err => console.log(err) && next({status_code: 500, status: "Error - Internal Error !", message: "Internal Error"}))
);

// maybe should not be a route but a cron function (webcron are easy)
router.get('/notify', em.requireAuth ,async (req, res, next) => 
  Echange.find({sciper: req.tequila.sciper, to_notify:1}).exec()
  .then(echange => {
    echange.forEach(ech=>{
      console.log("Mail : You're echange has been accepted, you're new group : "+ech.target);
    });
    res.json("OK");
  })
  .catch(err => console.log(err) && next({status_code: 500, status: "Error - Internal Error !", message: "Internal Error"}))
);

router.get('/me', em.requireAuth, (req,res,next)=>{
  res.json(req.tequila);
})

router.get('/status', em.requireAuth, async (req, res, next) => 
  Echange.findOne({sciper: req.tequila.sciper}).exec()
  .then( echange => res.json(echange))
  .catch(err => console.log(err) && next({status_code: 500, status: "Error - Internal Error !", message: "Internal Error"}))
);

router.get('/list', em.requireAdmin, async (req, res, next) =>
  Echange.find({}).exec()
  .then( all => res.json(all))
  .catch(err => console.log(err) && next({status_code: 500, status: "Error - Internal Error !", message: "Internal Error"}))
);

router.get('/stats', em.requireAdmin, async (req, res, next) => {
  //TODO: Static Placeholder currently
  res.json({
            total:{
              global:1500,
              A:{
                from:700,
                to: 300,
              },
              B:{
                from:300,
                to:500,
              },
              C:{
                from:500,
                to: 700,
              }
            },
            complete:{
              global:500,
              A:{
                from:700,
                to: 300,
              },
              B:{
                from:300,
                to:500,
              },
              C:{
                from:500,
                to: 700,
              }
            },
            incomplete:{
              global:1000,
              A:{
                from:500,
                to: 700,
              },
              B:{
                from:500,
                to: 700,
              },
              C:{
                from:500,
                to: 700,
              },
            }
  });
});

module.exports = router;