const mongoose = require('mongoose');

const EchangeSchema = new mongoose.Schema({
    sciper: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    target: {
        type: String,
        required: true,
        enum: ['0', '1', '2'],
    },
    section: {
        type: String,
        required: true,
    },    
    status: {
        type: String,
        required: true,
        enum: ['WISH', 'DONE'],
    },
    previous_group: {
        type: String,
        required: true,
        enum: ['0', '1', '2'],
    },
    to_notify: {
        type: Boolean,
        required: false,
    },
});

module.exports = mongoose.model('Echange', EchangeSchema);