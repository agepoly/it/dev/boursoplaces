const Echange = require('../models/echange');

const groups = ['0','1','2'];

module.exports = {
	requireAuth: (req,res,next) => {
		if(!(req.tequila))
			next({status_code: 401, status: "Error - Not Authenticated !", message: "Url requires authentication : "+req.originalUrl});
		else
			next();
	} ,
	requireAdmin: (req,res,next) => {
		if(!req.tequila || req.tequila.isAdmin == false)
			next({status_code: 401, status: "Error - Not Administrator !", message: "Url requires admin privileges : "+req.originalUrl});
		else
			next();
	},
	requireEXCHANGE_ENABLED: (req,res,next) => {
		if(process.env.BLOCK_ECHANGE === true)
	        return next({status_code: 400, status: "Error - Exchanges disabled !", message: "Exchanges are disabled at this moment"});
		else
			next();
	},
	requireValidGroup: (req,res,next) => {
		if(!groups.includes(req.query.target) || req.tequila.group == req.query.target)
			next({status_code: 400, status: "Error - Invalid group !", message: "Invalid Group selected"});
		else 
			next();
	},
	requireNoPreviousExchange: async (req,res,next) => {
		Echange.find({sciper: req.tequila.sciper}).exec()
		.then(echange =>{
			if(echange.length>0)
				next({status_code: 401, status: "Error - Already Exchanged !", message: "User already requested group change"});
			else
				next();
		}).catch(err=>next({status_code: 500, status: "Error - Internal Error !", message: "Internal Error"}));
	}
}